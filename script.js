const genFieldMatrix = (n) => {// orientation on the playing field is based on matrix with pass-through numeration from zero to last item
    let matrix = [];
    let cellIndex = 0;
    for (let r=0; r<n; r++) {
         let curRow = [];
         for (let c=0; c<n; c++){
                curRow.push(cellIndex);
                cellIndex++;
            }
         matrix.push(curRow);
    }
    return matrix
}

const genBombs = (n) => { // bombs generator picks randomly items within range of field matrix numeration
    let bomb;
    let bombArr = [];
    let cellQuan = Math.pow(n,2);
    let bombQuan = Math.floor(cellQuan/6);
    for (let i=0; i<bombQuan; i++) {
    do {bomb = Math.round(Math.random()*(cellQuan-1))}
    while (bombArr.some(item => item===bomb)) // "while" cycle prevents bombs duplication
    bombArr.push(bomb);
    }
    return bombArr
}

const genPlayField = (matrix, n, mines) => {// generating table to visualize the game
    let field = $(".field");
    let table = matrix.map(row =>`<tr>${row.map(cell => `<td id=${cell} class ="cell"></td>`)}</tr>`);
    field.append(table);
    genValuesList(matrix,n,mines);
}

const genValuesList = (matrix, n, minesArr) => {//based on generated bombs array function calculates nearby bombs
    let valuesList = [];                        //and puts number in "data-value" attribute of each cell
    let index = 0;  // comparison is based on each cell index generated during field matrix creation
    for (row = 0; row<n; row++) {       // as far as pass-through index numeration doesn't give full understanding of neighborhood cells (e.g cell 8 won't be near cell 7 in matrix 8x8 cause cell 8 will start a new row)
        for (col=0; col<n; col++) {     // row and col iteration is added to know cell relative position within matrix (matrix[row][col])
            if(minesArr.some((m)=> m===index)) { // if it is a bomb cell, marker 'X' is put in attribute
                $(`#${index}`).attr("data-value",`X`);
                valuesList.push(`X`)}
            else { // if it's not a bomb cell, we define number of bombs around and put it in attribute
                let area = defineArea(matrix,row,col,n);
                let content = getBombQuant(area,minesArr);
                $(`#${index}`).attr("data-value",`${content}`);
                valuesList.push(content);
            }
            index++
        }

    }
}

const defineArea = (matrix,row,col,n)=> {// based on current cell relevant position in matrix defines area 3x3 with current cell in the middle
    // function is used while generating value attributes for non-bombs cells, opening cells with 0 bombs around, revealing surround cells with dblclick
    let area = [];
    for (let r = -1; r <2; r++) {
        let curRow = row+r;// iterating rows previous -> current -> next one
        if (curRow >=0 && curRow <n) {//preventing row overflows at matrix edges
                for (let c = -1; c <2; c++) {// iterating cells within current row from previous to next
                    let curCol = col+c;
                    if (curCol >=0 && curCol < n) area.push(matrix[curRow][curCol])
                }
        }
    }
    return area
}


const getBombQuant = (area, bombs) => { //iterating all cells within area, checking each with bombs-array
    let quantity = 0;
    for (key in area) {
        if (bombs.some((bomb)=> bomb==area[key])) quantity++
    }
    return quantity
}


const cellOnClick = (event) => { // main user-interaction function
    cellId = $(event.target).attr("id");
    let clickedCell = `#${cellId}`;
    if (mines.some((mine)=> mine==cellId)) { // check if this is a bomb cell
        $("td[data-value='X']")
            .css("background","red")
            .html(`<i class="fab fa-sith"></i>`);
        $("td").off("click",cellOnClick); // turning off all actions on field
        $("td").off("dblclick",cellDblClick); // turning off all actions on field
        $("td").off("contextmenu",handleFlag); // turning off all actions on field
        if ($(".new-game").css("display")=="none") $(".new-game").fadeIn(300)
    }
    else if ($(clickedCell).attr("data-value") == "0") { // if no bombs around, nearby cells are revealed
        openZeroCells(clickedCell);
        $(clickedCell).addClass("activated");//class "activated is added" to understand when all necessary cells are revealed (+ a little bit of formatting in css)
    }
    else {
        let value = Number($(clickedCell).attr("data-value"));
        let numColorsArr = [0,"#0100fe","#017f01","#fe0000","#010080","#54171e","#1d796c","#brown","#5958c7"];
        $(clickedCell).text(value);
        $(clickedCell).css("color",`${numColorsArr[value]}`);// numColorsArr is the set of colors for numbers 1-8 that may be in cell
        $(clickedCell).addClass("activated");
    }
    if ($(".activated").length === 1) $(".new-game").fadeIn(300); // to show NEW GAME button after first move
    if ($(".activated").length === Math.pow(n,2) - mines.length) winGame(); // check on game end after each move
    $(clickedCell).off("contextmenu",handleFlag);//to avoid putting a flag on a cell that have already been clicked
}

const cellDblClick =()=> {
    cellId = $(event.target).attr("id");
    let clickedCell = `#${cellId}`;
    let position = findPosition(matrix,cellId);//findPosition returns cell relative address within matrix
    let area = defineArea (matrix, position[0], position[1],n);
    area = area.map((cell)=>`#${cell}`);//transforming array to jquery-friendly syntax
    let flg = 0;
    area.forEach((cell)=> { // defining number of flags put around current cell
        if($(cell).hasClass("flagged")) flg++
    });
    if (Number($(clickedCell).text())=== flg)
    area.forEach((cell)=> $(cell).trigger("click"));//if number of flags= cell value, triggering click on surrounding cells

}

const winGame = () => { // simple function for user to feel a taste of victory
    let message = $("<div>YOU WON!!!</div>")
    $(message).addClass("winner-message")
    $("td[data-value='X']")
                .html(`<i class="fas fa-bomb"></i>`);// showing bombs position
    $(".field").append(message);
    $("td").off("click",cellOnClick); // turning off all actions on field after victory
    $("td").off("dblclick",cellDblClick); // turning off all actions on field after victory
    $("td").off("contextmenu",handleFlag); // turning off all actions on field after victory
}


const openZeroCells = (clickedCell) => {//triggering click for all surrounding sells, used if cell value is 0
    let position = findPosition(matrix,cellId);
    let area = defineArea (matrix, position[0], position[1],n);
    area = area.map((cell)=>`#${cell}`);
    area.forEach((cell)=> $(cell).trigger("click"));
}

function handleFlag (event) { // ES5 function due to need of "this"
// function works as setter and remover of flag
    event.preventDefault();
    if ($(this).hasClass("flagged")===true) {
        $(this)
        .html("")
        .on("click",cellOnClick);
    }
    else {
    $(this)
    .html(`<i class="fab fa-font-awesome-flag"></i>`)
    .off("click",cellOnClick) // if flag is put, cell should be protected from clicking
    }
    $(this).toggleClass("flagged");
    $(".flags").text($(".flagged").length);// returns indication of put flags in game header
}

const findPosition = (matrix,number)=> {
// returns relative cell position within field matrix, used in right-click and double-click interactions
    for (let row in matrix) {
        for (let col in matrix) {
            if (matrix[row][col] == number) {
                let address = [Number(row),Number(col)];
                return address
            }
        }
    }
}

const startNewGame = () => {
    $(".playing-field").html(`<table class="field"></table>`);
    startGame(8);
}

const startCustomGame = () => {
    $(".playing-field").html(`<table class="field"></table>`);
    let rowsNumber = Number(prompt("Select the length of the field (e.g. if you put 10, field will be 10x10)"))
    startGame(rowsNumber);
}


const startGame = (scale) => {
    n = scale
    matrix = genFieldMatrix(n);
    console.log("Playing field:");//////////////////// I kept this log for your convenience to see my idea of matrix structure
    console.log(matrix);/////////////////////////////
    mines = genBombs(n);
    genPlayField(matrix,n,mines);
    $(".flags").text($(".flagged").length);// indicating # of flags put in game header
    $(".mines").text(mines.length);// indicating # of mines in game header
    $(".new-game").hide();// according to task, new game button must be hidden till first move
    $("td").one("click",cellOnClick);
    $("td").on("dblclick",cellDblClick);
    $("td").on("contextmenu",handleFlag);
    $(".new-game").one("click",startNewGame);
    $(".custom-game").one("click",startCustomGame);
}


let matrix,mines,n,fieldValues
startGame(8)// by default game starts 8x8